# Small test task for Densou Trading Desk

### Introduction

We have a couple of things that we would want to feel you out on.

 1. How well do you handle popular PHP frameworks? Do you use built-in functionality or do you write functionality yourself? Which tools do you use for different types of tasks.
 2. Architecture. Do you have a gut feeling for scaling applications.
 3. Current standards and best practice. Do you use them, and is your code easy for other developers to debug and maintain.

### What do we want?
##### Coding
A web application - in a framework of your choice - that has two pages.
The main page should be a form with an text field and a button. 
When we type in the name of a Github repository in the text field, and click the button, we want all the contributors to that repo, listed below with their name, picture (avatar) and number of contributions.
Example of a valid inputs for the text field are ```phalcon/cphalcon``` or ```laravel/laravel```.
It should be possible to sort the contributors, by number of contributions or name.
A secondary page should display recent searches - they should be stored in a database.

##### Writing
Write 10 lines about how this application could support fetching data from other sources than GitHub. Let us say we wanted to fetch data from Stack Overflow and LinkedIn too. How should this be done with tables in a database, organising code in classes in the project, etc.

### How we want it?

Please create a git repository that we can clone the project from.

*Thank you*
